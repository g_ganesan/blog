class Comment < ActiveRecord::Base
	#add association
	belongs_to :post
	
	#add validations
	validates_presence_of :post_id, :only_integer => true
	validates_presence_of :body
end
