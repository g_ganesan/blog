class Post < ActiveRecord::Base
	#add association
	has_many :comments, dependent: :destroy
	
	#add validations
	validates_presence_of :title
	validates_presence_of :body
end
